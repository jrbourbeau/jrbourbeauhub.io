---
layout: single
title: R&eacute;sum&eacute;
permalink: /resume/
author_profile: true
type: pages
scope:
    path: ""
    type: pages
toc: true
toc_label: "R&eacute;sum&eacute;"
---

## Technical Skills
<div>
    <p>I am experienced with:</p>
    <ul>
        <li>
            <p>Python programming</p>
        </li>
        <li>
            <p>Machine learning (scikit-learn, xgboost, keras, etc.)</p>
        </li>
        <li>
            <p>Scientific computing (numpy, pandas, etc.)</p>
        </li>
        <li>
            <p>Distributed and parallel computing (dask, HTCondor)</p>
        </li>
        <li>
            <p>Data visualization (matplotlib, seaborn)</p>
        </li>
        <li>
            <p>Version control systems (git)</p>
        </li>
    </ul>
</div>


## Education
<!-- <div>
    <ul>
        <li>
            <p> PhD, Physics (in progress)
                <br><i>University of Wisconsin&mdash;Madison</i>
                <ul>
                    <li>Advisor: Stefan Westerhoff</li>
                </ul>
            </p>
        </li>
        <li>
            <p> MS, Physics
                <br><i>University of Wisconsin&mdash;Madison</i>
            </p>
        </li>
        <li>
            <p> Honors BS, Physics
                <br><i>University of Texas at Arlington</i>
                <ul>
                    <li>Minor: Mathematics</li>
                    <li>Summa Cum Laude</li>
                </ul>
            </p>
        </li>
    </ul>
</div> -->

PhD, Physics (in progress)<br>
University of Wisconsin&mdash;Madison

MS, Physics<br>
University of Wisconsin&mdash;Madison

Honors BS, Physics<br>
University of Texas at Arlington


## Selected Talks

"Supervised machine learning in Python with scikit-learn"<br>
Madison Python meetup (MadPy)&mdash;Madison, WI. March 2018.<br>
[ [Slides](https://jrbourbeau.github.io/madpy-ml-sklearn-2018/) ] [ [GitHub repo](https://github.com/jrbourbeau/madpy-ml-sklearn-2018) ]


"Introduction to the shell and Python"<br>
IceCube Software Bootcamp&mdash;Madison, WI. June 2016.<br>

"Dark Matter Searches with a Mono-Z' Jet"<br>
Phenomenology 2015 Symposium&mdash;Pittsburgh, PA. May 2015.

"Development of a Fast Timing System for the ATLAS Forward Proton Detector"<br>
UT-Arlington Annual Celebration of Excellence by Students. March 2012.
