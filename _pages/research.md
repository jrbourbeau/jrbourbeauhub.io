---
layout: single
title: Research
permalink: /research/
author_profile: true
type: pages
---

## Cosmic-ray Composition

![image-left](/assets/images/bdt-small.png){: .align-right} Cosmic rays are charged particles
(e.g. protons, alpha particles, iron nuclei, etc.) originating from outer space. I focus on
 applying machine learning techniques to data collected with the IceCube South Pole Neutrino Observatory to study the composition of cosmic-rays at high energies. A better
  understanding of the cosmic-ray composition can help answer fundamental questions related to the origin of cosmic rays. More information can be found on the [GitHub](https://github.com/jrbourbeau/cr-composition) page for this analysis.
{: .text-left}


## Dark Matter Phenomenology

Previously I worked with Yang Bai in the high energy phenomenology group on investigating a specific dark matter signature produced by Z' jets at the Large Hadron Collider (LHC). We, along with our collaborator Tongyan Lin, published a paper on this topic in the Journal for High Energy Physics. [arXiv:1504.01395](https://arxiv.org/abs/1504.01395).

> Y. Bai, J. Bourbeau, and T. Lin. "Dark matter searches with a mono-Z' jet," JHEP 06, 205 (2015). DOI: 10.1007/JHEP06 (2015) 205.

{% include feature_row id="feature_row3" type="left" %}
