---
layout: single
title: Software
permalink: /software/
author_profile: true
type: pages
scope:
    path: ""
    type: pages
---

I am an active developer, maintainer, and contributor to several projects in the Python data science community. See my [GitHub profile](http://github.com/jrbourbeau) for
details.

## Maintainer

### PyCondor

Python API for submitting tasks to an HTCondor distributed cluster.
- [GitHub](https://github.com/jrbourbeau/pycondor)
- [Documentation](https://jrbourbeau.github.io/pycondor/)

### decotools

Python package to help analyze data collected by the Distributed Electronic Cosmic-ray Observatory.
- [GitHub](https://github.com/WIPACrepo/decotools)
- [Documentation](https://WIPACrepo.github.io/decotools/)


## Contributor

- [scikit-learn](https://github.com/scikit-learn/scikit-learn) &mdash; Python machine learning library.
- [dask](https://github.com/dask/dask) &mdash; Flexible parallel computing library.
- [mlxtend](https://github.com/rasbt/mlxtend) &mdash; Extension and helper modules for Python's data analysis and machine learning libraries.
